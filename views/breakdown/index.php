<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BreakdownSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Breakdowns';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="breakdown-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Breakdown', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            [
				'attribute' => 'statusId',
				'value' => 'findStatus.name',
				'filter' => [  1 => 'open', 2 => 'repair', 3 => 'closed',]
			],
            [
				'attribute' => 'levelId',
				'value' => 'findLevel.name',
				'filter' => [  1 => 'low', 2 => 'high', 3 => 'urgent',]
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
